package com.hoso0003.cz2001.lab2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Hashing {
	protected static String linearProbingHashTable[];
	protected static String doubleHashingHashTable[];
	
	private static boolean initialized = false;
	private static ArrayList<String> list;
	private static int testSize;
	private static Scanner sc;

	public static void main(String[] args){
		int choice, size;
		boolean dataGenerated = false;
		sc = new Scanner(System.in);

		linearProbingHashTable = new String[1000];
		doubleHashingHashTable = new String[1000];

		int[] testSizeList = {100, 300, 500, 700, 900};

		boolean passed = false;

		do{
			System.out.println("Please select action");
			System.out.println("1. Generate data");
			System.out.println("2. Input test case");
			System.out.println("3. Print table");
			System.out.println("4. Test");
			System.out.println("5. Quit");
			choice = sc.nextInt();
			switch(choice){
			case 1:
				System.out.print("Please enter data size {100, 300, 500, 700, 900}: ");
				size = sc.nextInt();
				sc.nextLine();
				passed = false;
				do{
					try {
						generateRandomUniqueData(size);
						passed=true;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
				}while(!passed);
				dataGenerated = true;
				break;
			case 2:
				if(!dataGenerated){
					System.out.println("Please generate data first.");
					break;
				}
				sc.nextLine();
				System.out.println("Enter the matric number that you want to search:");
				String m = sc.nextLine();
				searchData(m);
				break;
			case 3: 
				if(!dataGenerated){
					System.out.println("Please generate data first.");
					break;
				}
				printTable(); 
				break;
			case 4:

				if(!dataGenerated){
					System.out.println("Please generate data first.");
					break;
				}
				System.out.print("Number of test cases: ");
				testSize = sc.nextInt();
				sc.nextLine();
				searchData();
				break;
			default:break;
			}
		}while(choice != 5);
	}

	public static ArrayList<String> generateRandomNumber(){
		if(!initialized){
			list = new ArrayList<String>();
			for (int i=1400000; i<1500000; i++) 
				list.add("U"+i+"A");
			initialized = true;
		}
		ArrayList<String> l = (ArrayList<String>) list.clone();
		Collections.shuffle(l);
		return l;
	}

	public static void generateRandomUniqueData(int size) throws Exception{
		for(int i=0; i<1000; i++){
			linearProbingHashTable[i] = "-1";
			doubleHashingHashTable[i] = "-1";
		}
		ArrayList<String> list = generateRandomNumber();
		for (int i=0; i<size; i++)
			insertIntoHashTable(list.get(i));
	}

	public static void insertIntoHashTable(String r) throws Exception{
		// LinearProbingHashing
		int key,loc,inc;
		key = hashing(r.hashCode());
		loc = key;
		//if table empty
		if(!linearProbingHashTable[key].equalsIgnoreCase("-1")){
			do{
				key = hashing(key+1);
			}while(key != loc && !linearProbingHashTable[key].equalsIgnoreCase("-1"));
			if (key == loc)
				throw new Exception();
		}
		linearProbingHashTable[key] = r;

		// DoubleHasing
		key = loc;
		inc = hashInc(r.hashCode());
		if(!doubleHashingHashTable[key].equalsIgnoreCase("-1")){
			key = hashing(key+inc);
			while(key != loc && !doubleHashingHashTable[key].equalsIgnoreCase("-1")){
				key = hashing(key+inc);
			}
			if (key == loc)
				throw new Exception();
		}
		doubleHashingHashTable[key] = r;
	}

	public static int hashing(int r){
		return r % 997;
	}

	public static int hashInc(int r){
		return r % 887 + 1;
	}

	public static void printTable(){
		System.out.println("Index\t| LP\t\t| DH");
		for(int i=0; i<1000; i++){
			System.out.print(i+"\t| ");
			if(!linearProbingHashTable[i].equalsIgnoreCase("-1"))
				System.out.print(linearProbingHashTable[i]+ "\t| ");
			else
				System.out.print("-\t\t| ");
			if(!doubleHashingHashTable[i].equalsIgnoreCase("-1"))
				System.out.print(doubleHashingHashTable[i]);
			else
				System.out.print("-");
			System.out.print("\n");
		}
	}

	public static void searchData(String num){
		long timeUsedForFoundLP = 0;
		long timeUsedForNotFoundLP = 0;
		int comparisonForFoundLP = 0;
		int comparisonForNotFoundLP = 0;

		long timeUsedForFoundDH = 0;
		long timeUsedForNotFoundDH = 0;
		int comparisonForFoundDH = 0;
		int comparisonForNotFoundDH = 0;

		long beginTime, endTime;
		int found = 0;
		int key, loc, inc, comparison;

		inc= hashInc(num.hashCode());
		key = hashing(num.hashCode());
		loc = key;
		// Linear Probing
		comparison = 1;
		beginTime = System.nanoTime();
		if(!linearProbingHashTable[key].equalsIgnoreCase("-1") && !linearProbingHashTable[key].equalsIgnoreCase(num)){
			do{
				comparison++;
				key = hashing(key+1);
			}while (!linearProbingHashTable[key].equalsIgnoreCase("-1") && !linearProbingHashTable[key].equalsIgnoreCase(num) && key != loc);
		}
		endTime = System.nanoTime();
		if(linearProbingHashTable[key].equalsIgnoreCase(num)){
			comparisonForFoundLP += comparison;
			timeUsedForFoundLP += endTime - beginTime;
			found++;
		}else{
			comparisonForNotFoundLP += comparison;
			timeUsedForNotFoundLP += endTime - beginTime;
		}
		// Double Hashing
		comparison = 1;
		key = loc;
		beginTime = System.nanoTime();
		if(!doubleHashingHashTable[key].equalsIgnoreCase("-1") && !doubleHashingHashTable[key].equalsIgnoreCase(num)){
			do{
				comparison++;
				key = hashing(key+inc);
			}while (!doubleHashingHashTable[key].equalsIgnoreCase("-1") && !doubleHashingHashTable[key].equalsIgnoreCase(num) && key != loc);
		}
		endTime = System.nanoTime();
		if(doubleHashingHashTable[key].equalsIgnoreCase(num)){
			comparisonForFoundDH += comparison;
			timeUsedForFoundDH += endTime - beginTime;
		}else{
			comparisonForNotFoundDH += comparison;
			timeUsedForNotFoundDH += endTime - beginTime;
		}
		if(found >= 1)
			System.out.println("\n\nFound "+num+"\n");
		else
			System.out.println("\n\nNOT Found "+num+"\n");
		System.out.println("----------------------");
		System.out.println("Linear Probing Hashing");
		System.out.println("----------------------");
		if(found >= 1){
			System.out.println("FOUND: Average CPU time = " + timeUsedForFoundLP + " nanoseconds");
			System.out.println("FOUND: Number of Comparison : "+ comparisonForFoundLP);
		}else{
			System.out.println("NOT FOUND: Average CPU time = " + timeUsedForNotFoundLP + " nanoseconds");
			System.out.println("NOT FOUND: Number of Comparison : "+ comparisonForNotFoundLP);
		}
		System.out.println("--------------");
		System.out.println("Double Hashing");
		System.out.println("--------------");
		if(found >= 1){
			System.out.println("FOUND: Average CPU time = " + timeUsedForFoundDH + " nanoseconds");
			System.out.println("FOUND: Number of Comparison : "+ comparisonForFoundDH);
		}else{
			System.out.println("NOT FOUND: Average CPU time = " + timeUsedForNotFoundDH + " nanoseconds");
			System.out.println("NOT FOUND: Number of Comparison : "+ comparisonForNotFoundDH);
		}
	}

	public static void searchData(){
		long timeUsedForFoundLP = 0;
		long timeUsedForNotFoundLP = 0;
		int comparisonForFoundLP = 0;
		int comparisonForNotFoundLP = 0;

		long timeUsedForFoundDH = 0;
		long timeUsedForNotFoundDH = 0;
		int comparisonForFoundDH = 0;
		int comparisonForNotFoundDH = 0;

		long beginTime, endTime;
		int found = 0;
		String num;
		int key, loc, inc, comparison;
		ArrayList<String> list = generateRandomNumber();
		for(int i=0; i<testSize; i++){
			int j = i % 100000;
			num = list.get(j);
			inc= hashInc(num.hashCode());
			key = hashing(num.hashCode());
			loc = key;
			// Linear Probing
			// Best case 	: 1
			//			 	: When the matric number is found at location = key
			//			 	: or matric number is not found at location = key and it is empty at location = key
			//			 	: O(1)
			//
			// Worst case	: data size, n
			//			 	: When all the data are next to each other and the matric number is either the last element or not found
			//			 	: O(n)
			//
			// Average case	: (1+2+...+n-1+n)/n
			//			   	:   n*(n+1)
			//			   	: = ------- = O(n)
			//			   	:     2*n
			comparison = 1;
			beginTime = System.nanoTime();
			if(!linearProbingHashTable[key].equalsIgnoreCase("-1") && !linearProbingHashTable[key].equalsIgnoreCase(num)){
				do{
					comparison++;
					key = hashing(key+1);
				}while (!linearProbingHashTable[key].equalsIgnoreCase("-1") && !linearProbingHashTable[key].equalsIgnoreCase(num) && key != loc);
			}
			endTime = System.nanoTime();
			if(linearProbingHashTable[key].equalsIgnoreCase(num)){
				comparisonForFoundLP += comparison;
				timeUsedForFoundLP += endTime - beginTime;
				found++;
			}else{
				comparisonForNotFoundLP += comparison;
				timeUsedForNotFoundLP += endTime - beginTime;
			}
			// Double Hashing
			// Best case 	: 1
			//			 	: When the matric number is found at location = key
			//			 	: or matric number is not found at location = key and it is empty at location = key
			//			 	: O(1)
			//
			// Worst case	: data size, n
			//			 	: When all the data are next to each other and the matric number is either the last element or not found
			//			 	: O(n)
			//
			// Average case	: (1+2+...+n-1+n)/n
			//			   	:   n*(n+1)
			//			   	: = ------- = O(n)
			//			   	:     2*n
			comparison = 1;
			key = loc;
			beginTime = System.nanoTime();
			if(!doubleHashingHashTable[key].equalsIgnoreCase("-1") && !doubleHashingHashTable[key].equalsIgnoreCase(num)){
				do{
					comparison++;
					key = hashing(key+inc);
				}while (!doubleHashingHashTable[key].equalsIgnoreCase("-1") && !doubleHashingHashTable[key].equalsIgnoreCase(num) && key != loc);
			}
			endTime = System.nanoTime();
			if(doubleHashingHashTable[key].equalsIgnoreCase(num)){
				comparisonForFoundDH += comparison;
				timeUsedForFoundDH += endTime - beginTime;
			}else{
				comparisonForNotFoundDH += comparison;
				timeUsedForNotFoundDH += endTime - beginTime;
			}
		}
		//cpu time
		System.out.println("\n\nTest case: "+testSize+"; Found: "+found+"; Not found: "+(testSize-found)+"\n");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("\t| Linear Probing Hashing\t| Double Hashing");
		System.out.println("----------------------------------------------------------------------");
		System.out.println("Type\t| CPU (ns)\t| Comparison\t| CPU (ns)\t| Comparison");
		System.out.println("----------------------------------------------------------------------");
		if(found>0)
			System.out.println("FOUND\t| "+(timeUsedForFoundLP/found)+"\t\t| "+comparisonForFoundLP+"\t\t| "+(timeUsedForFoundDH/found)+"\t\t| "+comparisonForFoundDH);
		if(found!=testSize)
			System.out.println("XFOUND\t| "+(timeUsedForNotFoundLP/(testSize-found))+"\t\t| "+comparisonForNotFoundLP+"\t\t| "+(timeUsedForNotFoundDH/(testSize-found))+"\t\t| "+comparisonForNotFoundDH);
		System.out.println("----------------------------------------------------------------------");
	}
}
