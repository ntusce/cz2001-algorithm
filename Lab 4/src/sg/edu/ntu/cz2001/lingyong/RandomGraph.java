package sg.edu.ntu.cz2001.lingyong;
import java.util.*;
//!!
//Right mouse click - Run As - Run Configuration - Arguments - Vm Arguments, then add this -Xmx5048m
public class RandomGraph {
	
	//variables
	static int eSize[] = {1000, 5000, 10000, 50000, 100000};
	static int vSize[] = {500, 1000};// temporary set small default value 5000,10000
	static int[][] visited = new int[10][];
	static int[][][] preD = new int [10][][];// predecessor

    static long timeUsed = 0;
    static long beginTime, endTime = 0;
	
    // actual graph with connections
	static ArrayList<ArrayList<Integer>> adjLists = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists2 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists3 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists4 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists5 = new ArrayList<ArrayList<Integer>>();
    
	static ArrayList<ArrayList<Integer>> adjLists6 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists7 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists8 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists9 = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<Integer>> adjLists10 = new ArrayList<ArrayList<Integer>>();
	
	static int[] randomize(int vertice,ArrayList<ArrayList<Integer>> adjLists)
	{
	   int range = (vertice) ;
	   int random[] = new int[2];//start point/end point
	   boolean contains = true;
	   while(random[0] == random[1] || (contains == true))//not same vertice and not same edge
	   {
		   random[0] = (int)(Math.random() * range);
		   random[1] = (int)(Math.random() * range);
		   if(adjLists.get(random[0]).contains(random[1]) == true)// check for same edge
		   {
			   contains = true;
		   }
		   else{
			   contains = false;
		   }
	   }
	   
	   return random;
	   
	}
	static void initialize(ArrayList<ArrayList<Integer>> adjLists,int vSize,int eSize,int u,int v)
	{
		int count = 0;
		while(count < eSize)
        {
        	int array1[] = randomize(vSize,adjLists);
        	
        	//undirected/bi
        	adjLists.get(array1[0]).add(array1[1]);
        	adjLists.get(array1[1]).add(array1[0]);
        	count += 2;
        }
	}
	static void init1()
	{
        int n = vSize[1];
		for(int i=0; i<n; i++){
        	if(i<vSize[0])
        		adjLists.add(new ArrayList<Integer>());
        	if(i<vSize[0])
                adjLists2.add(new ArrayList<Integer>());
        	if(i<vSize[0])
                adjLists3.add(new ArrayList<Integer>());
        	if(i<vSize[0])
                adjLists4.add(new ArrayList<Integer>());
        	if(i<vSize[0])
                adjLists5.add(new ArrayList<Integer>());
        	
        		adjLists6.add(new ArrayList<Integer>());
                adjLists7.add(new ArrayList<Integer>());
                adjLists8.add(new ArrayList<Integer>());
                adjLists9.add(new ArrayList<Integer>());
                adjLists10.add(new ArrayList<Integer>());
        }
		
		for(int i = 0 ; i < 5 ; i++)
		{
			visited[i] = new int[vSize[0]];//vSize 5k
			visited[i+5] = new int[vSize[1]];//vSize 10k

		}
		
		initialize(adjLists, vSize[0], eSize[0],0,0);
        initialize(adjLists2, vSize[0], eSize[1],0,1);
        initialize(adjLists3, vSize[0], eSize[2],0,2);
        initialize(adjLists4, vSize[0], eSize[3],0,3);
        initialize(adjLists5, vSize[0], eSize[4],0,4);
        
        initialize(adjLists6, vSize[1], eSize[0],5,5);
        initialize(adjLists7, vSize[1], eSize[1],5,6);
        initialize(adjLists8, vSize[1], eSize[2],5,7);
        initialize(adjLists9, vSize[1], eSize[3],5,8); /**/
        initialize(adjLists10, vSize[1], eSize[4],5,9);
	}
	static void BFS(ArrayList<ArrayList<Integer>> adjLists, int s,int u,int v)// u == data set v = visited s = source
	{
		Queue L = null;
		visited[v][s] = 1;//visited
		L = new LinkedList();
		L.add(s);
		int count = 1;
		while(L.peek()!= null)
		{
			int currentVertex = (int) L.poll();//pop
			
			for(int w : adjLists.get(currentVertex))// these are neighbors
			{
				
				if(visited[v][w] == 0)
				{
					visited[v][w] = 1;
					preD[u][s][w] = currentVertex;
					L.add(w);
					count++;
				}
			}
		}
		//System.out.println("Count = "+count);
	}
	static void bfsSweep(ArrayList<ArrayList<Integer>> adjLists,int size,int u,int v)//u == eSizeArray //v == visited
	{ 
		int p = 0;
		for(int i = 0; i<size;i++)
		{
			visited[v][i] = 0;
		}
		
		for(int s = 0; s <size; s++)
		{
			//it sweeps through everything here multiple edges on single vertex
			for(int j = 0; j<size;j++)
			{
				visited[v][j] = 0;
			}
			//if(visited[v][i] == 0)
			//{
				BFS(adjLists,s,u,v);
			//}
		}
	}
	static void searchShortestPath(int p , int q,int[] preD)
	{
		int currentStart = q;
		int length1 = 0;
		boolean found = false;
		while(preD[currentStart] != -1 && preD[currentStart] != p)
		{
			currentStart = preD[currentStart];
		}
		
		if(preD[currentStart] == p)//path is found
		{
			currentStart = q;
			System.out.print(currentStart + "<-");
			while(preD[currentStart] != -1 && preD[currentStart] != p)
			{
				System.out.print(preD[currentStart] + "<-");
				currentStart = preD[currentStart];
				
				length1++;
				
			}
			System.out.println(preD[currentStart]);
			length1++;
			System.out.println("Shortest Path's length is "+ length1);
			found = true;
		}
		if(found)
		{
			currentStart = q;
		}
		else
			System.out.println("Shortest path is not available");
		
	}
	static void startBFS()
	{
		System.out.println("Edge\t|Vertex\t|Time(ms)");
    	beginTime = System.currentTimeMillis();	
    	bfsSweep(adjLists, vSize[0],0,0);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("1000\t| 5000\t|"+timeUsed);
        
    	beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists2, vSize[0],1,1);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("5000\t| 5000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists3, vSize[0],2,2);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("10000\t| 5000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists4, vSize[0],3,3);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("50000\t| 5000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists5, vSize[0],4,4);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("100000\t| 5000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists6, vSize[1],5,5);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("1000\t| 10000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists7, vSize[1],6,6);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("5000\t| 10000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists8, vSize[1],7,7);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("10000\t| 10000\t|"+timeUsed);
        
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists9, vSize[1],8,8);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("50000\t| 10000\t|"+timeUsed);
        /**/
        beginTime = System.currentTimeMillis();
    	bfsSweep(adjLists10, vSize[1],9,9);
    	endTime = System.currentTimeMillis();
        timeUsed = (endTime - beginTime);
        System.out.println("100000\t| 10000\t|"+timeUsed);
	}
	static void reset()
	{
		for(int k = 0; k<5; k++)
		{
			for(int i = 0 ; i < vSize[0];i++)
			{
				for(int j = 0 ; j < vSize[0];j++)
				{
				preD[k][i][j] = -1;
				}
			}
			
			for(int i = 0 ; i < vSize[1];i++)
			{
				for(int j = 0 ; j < vSize[1];j++)
				{
				preD[k+5][i][j] = -1;
				}
			}
		}
		
	}
	public static void main(String[] args)
	{
		
		//Out of memory problems
		for(int i = 0 ; i < 5; i++)
		{
			preD[i] = new int[vSize[0]][vSize[0]];//vSize 5k 25000000 first 5 use this
			preD[i+5] = new int[vSize[1]][vSize[1]];//vSize 10k 100m last 5 use this
		}
		
		reset();//set every value in preD to -1
		init1();// initialize everything including making the graph
		startBFS();// start breadth first search on graphs
		
        /*
        System.out.println("Vertex:");
        for(int v=0; v<vSize[0]; v++){
        	System.out.println(v + ": " + adjLists.get(v));
        }
         
         for printing the relationship
         for(int v=0; v<vSize[0]; v++){
            	 System.out.println(v+" : "+preD[0][0][v]);
          }
         */
        
        	Scanner sc = new Scanner(System.in);
    		int choice;
			do {
				
    			System.out.println("Perform the following methods :");
    			System.out.println("(1) Search for shortest path between both vertices");
    			System.out.println("(2) Exit");
    			System.out.print("Enter the number of your choice: ");
    			choice = sc.nextInt();
    			switch (choice) {
    			case 1: 
    				int p;
    				int q;
    				int r;
    				System.out.println("1) Edge Size 1000 Vertex Size 5000 ");
    				System.out.println("2) Edge Size 5000 Vertex Size 5000 ");
    				System.out.println("3) Edge Size 10000 Vertex Size 5000 ");
    				System.out.println("4) Edge Size 50000 Vertex Size 5000 ");
    				System.out.println("5) Edge Size 100000 Vertex Size 5000 ");
    				System.out.println("6) Edge Size 1000 Vertex Size 10000 ");
    				System.out.println("7) Edge Size 5000 Vertex Size 10000 ");
    				System.out.println("8) Edge Size 10000 Vertex Size 10000 ");
    				System.out.println("9) Edge Size 50000 Vertex Size 10000 ");
    				System.out.println("10) Edge Size 100000 Vertex Size 10000 ");
    				System.out.println("Which dataset (1-10): ");
    				
    				
    			
    				r = sc.nextInt();
    				/*
    		        System.out.println("Vertex:");
    		        for(int v=0; v<vSize[0]; v++){
    		        	System.out.println(v + ": " + adjLists.get(v));
    		        }
    		        set to appropriate adjList to see the graph
    		        */
    				
    				System.out.println("Starting vertex (smaller number): ");
    				p = sc.nextInt();
    				
    				for(int v=0; v<vSize[0]; v++){
   	            	 System.out.println(v+" : "+preD[r-1][p][v]);
   	            }
    				
    				System.out.println("Ending vertex (bigger number): ");
    				q = sc.nextInt();
    				
    				searchShortestPath(p,q,preD[r-1][p]);
    				break;
    			case 2: System.out.println("Program terminating ….");
    			}
    		}while (choice < 2);
		
	}

}

