package sg.edu.ntu.cz2001.lab5;

import java.util.Random;

public class Graph {

	protected int size;
	protected int edgesForPrinting;
	protected List[] lists;
	protected boolean[] visited;
	public Graph(int size, int edges){
		this.size = size;
		this.edgesForPrinting = edges;
		lists = new List[size];
		visited = new boolean[size];
		for(int j=0; j<size; j++){
			visited[j] = false;
			lists[j] = new List(j);
		}
		generateVerticesForEdges(edges);
	}
	
	public boolean generateVerticesForEdges(int edges){
		int count = 0, l, r;
		Random rand = new Random();
		while(count < edges){
			l = rand.nextInt(this.size);
			r = rand.nextInt(this.size);
			if(l == r || lists[l].hasNeighbour(r))
				continue;
			if(!lists[l].addKey(r) || !lists[r].addKey(l)){
				lists[l].removeKeyIfExist(r);
				lists[r].removeKeyIfExist(l);
				continue;
			}
			count++;
		}
		return true;
	}
	
	public void printShortestPathToNode(Node lastNode){
		if(lastNode == null)
			return;
		printShortestPathToNode(lastNode.getNext());
		System.out.print("->"+lastNode.getKey());
	}
	
	public int count(Node lastNode){
		if(lastNode == null)
			return 0;
		return 1+count(lastNode.getNext());
	}
	
	public int BFS(int s, int t){
		reset();
		Queue<Node> l = new Queue<Node>();
		Tree tree = new Tree(s);
		l.enqueue(tree.getCurrentPointer());
		while(!l.isEmpty()){
			Node thisNode = l.dequeue();
			Integer thisNodeKey = thisNode.getKey();
			tree.setCurrentPointer(thisNode.getParent());
			if(thisNodeKey == t){
//				printShortestPathToNode(thisNode);
				return count(thisNode);
			}
			if(visited[thisNodeKey])
				continue;
			visited[thisNodeKey] = true;
			for(Integer neighbour: lists[thisNodeKey].getAllNeighbour()){
				if(visited[neighbour])
					continue;
				l.enqueue(tree.addChildren(neighbour, thisNode));
			}
		}
		return -1;
	}
	
	public void DFSSweep(int s){
		reset();
		DFS(s);
	}
	
	public void DFS(int s){
		visited[s] = true;
		for(Integer i: lists[s].getAllNeighbour()){
			if(!visited[i]){
				System.out.println(i);
				DFS(i);
				return;
			}
		}
	}
	
	public void reset(){
		for(int j=0; j<size; j++){
			visited[j] = false;
		}
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List[] getLists() {
		return lists;
	}

	public void setLists(List[] lists) {
		this.lists = lists;
	}

	public boolean[] getVisited() {
		return visited;
	}

	public void setVisited(boolean[] visited) {
		this.visited = visited;
	}

	public int getEdgesForPrinting() {
		return edgesForPrinting;
	}
	
}
