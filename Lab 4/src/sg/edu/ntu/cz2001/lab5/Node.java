package sg.edu.ntu.cz2001.lab5;

public class Node {
	protected int key;
	protected Node next;
	protected Node parent;

	public Node(int key){
		this.key = key;
		next = null;
	}
	
	public Node(int key, Node parent){
		this.key = key;
		next = null;
		this.parent = parent;
	}
	
	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}
	
	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public boolean addNextNode(int key){
		this.next = new Node(key);
		return true;
	}
}
