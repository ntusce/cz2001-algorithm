package sg.edu.ntu.cz2001.lab5;

public class Tree {
	protected Node head;
	protected Node currentPointer;
	
	public Tree(int key){
		this.head = new Node(key);
		this.currentPointer = this.head;
	}
	
	public Node getHead() {
		return head;
	}

	public void setHead(Node head) {
		this.head = head;
	}

	public Node getCurrentPointer() {
		return currentPointer;
	}

	public void setCurrentPointer(Node currentPointer) {
		this.currentPointer = currentPointer;
	}

	public Node addChildren(int key, Node parent){
		Node newNode = new Node(key, parent);
		newNode.setNext(parent);
		return newNode;
	}
}
