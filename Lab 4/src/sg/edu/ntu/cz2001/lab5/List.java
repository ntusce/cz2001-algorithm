package sg.edu.ntu.cz2001.lab5;

import java.util.ArrayList;

public class List {
	protected int key;
	protected Node head;
	
	public List(int key){
		this.key = key;
		head = null;
	}
	
	public boolean hasNeighbour(int neighbour){
		if(neighbour == this.key)
			return false;
		Node n = head;
		if(n == null){
			return false;
		}
		if(n.getKey() == neighbour)
			return true;
		do{
			n = n.getNext();
			if(n == null)
				return false;
			if(n.getKey() == neighbour)
				return true;
		}while(n.getNext()!=null);
		return false;
	}
	
	public boolean addKey(int neighbour){
		if(neighbour == this.key)
			return false;
		if(hasNeighbour(neighbour))
			return false;
		Node p = head;
		if(p == null){
			head = new Node(neighbour);
			return true;
		}
		do{
			if(p.getNext() != null){
				p.addNextNode(neighbour);
				return true;
			}
			p = p.getNext();
		}while(p!=null); // not necessary
		return true;
	}
	
	public ArrayList<Integer> getAllNeighbour(){
		ArrayList<Integer> l = new ArrayList<Integer>();
		Node n = this.head;
		while(n != null){
			l.add(n.getKey());
			n = n.getNext();
		}
		return l;
	}
	
	public boolean removeKeyIfExist(int key){
		if(key == this.key)
			return false;
		Node n = head;
		if(n == null){
			return false;
		}
		if(n.getKey() == key){
			head = null;
			return true;
		}
		do{
			n = n.getNext();
			if(n==null)
				return true;
			if(n.getKey() == key){
				n.setNext(n.getNext().getNext());
				return true;
			}
		}while(n.getNext()!=null);
		return false;
	}
}
