package sg.edu.ntu.cz2001.lab5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import javax.rmi.CORBA.Util;

public class MainActivity {

	public static void main(String[] args) {
		int[] vertices = {5000, 10000};
		int[] edges = {1000, 5000, 10000, 50000, 100000};
		HashMap<Integer, ArrayList<Double>> results = new HashMap<Integer, ArrayList<Double>>();
		Graph[] gs = new Graph[vertices.length*edges.length];
		for(int v=0; v<vertices.length; v++){
			for(int e=0; e<edges.length; e++){
				gs[v*edges.length+e] = new Graph(vertices[v], edges[e]);
			}
		}
		for(int gi=0; gi<gs.length; gi++){
			Graph currentGraph = gs[gi];
			for(int i=0; i<currentGraph.getSize(); i++){
				for(int j=0; j<currentGraph.getSize() && j!=i; j++){
					Long startTime = System.nanoTime();
					int length = currentGraph.BFS(j, i);
					Long endTime   = System.nanoTime();
					Long totalTime = endTime - startTime;
//					if(length>0)
//					System.out.println(totalTime);
			    	if(!results.containsKey(length)){
			    		results.put(length, new ArrayList<Double>());
			    	}
			    	results.get(length).add(totalTime.doubleValue());
				}
			}
			System.out.println("Graph of vertices "+currentGraph.getSize()+" and edges "+currentGraph.getEdgesForPrinting());
			printResults(results);
		}
	}
	
	public static void printResults(HashMap<Integer, ArrayList<Double>> results){
		Set<Integer> lengthResult = results.keySet();
		ArrayList<Integer> lengths = new ArrayList<Integer>();
		lengths.addAll(lengthResult);
		Collections.sort(lengths);
		for(int x=0; x<=lengths.get(lengths.size()-1); x++){
			System.out.print(x+"\t");
			if(!lengths.contains(x)){
				System.out.println("");
				continue;
			}
			Double average = 0.0;
			for(Double o: results.get(x)){
				average+=o;
			}
			average/=results.get(x).size();
			for(int y=0;y<average/10;y++){
				System.out.print("X");
			}
			System.out.println(" (Average "+average+" of sample "+results.get(x).size()+")");
		}
	}

}
